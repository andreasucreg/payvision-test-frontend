import { Component, Input, OnInit } from '@angular/core';
import { TransactionsService } from '../../providers/transactions.service';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  
  transactions: any;
  selected: any;
  currencyCode: string = '-1';
  orderBy: string = '-1';
  action: string = '-1';
  loading: any;
  
  constructor(
    private transactionsService: TransactionsService
  ) {
    this.loading = true;
  }

	ngOnInit() {
    this.fetchTransactions();
  }

  onSubmit(form) {
    this.transactions = [];
    this.loading = true;
    this.transactionsService.transactionList(form.value.action, form.value.currencyCode, form.value.orderBy).then(data => {
      this.transactions = data;
      this.loading = false;
    });
  }

  onToggle(index) {
    if ( this.selected === index) {
      this.selected = -1;
    } else {
      this.selected = index;
    }
  }
  
  fetchTransactions() {
    this.transactionsService.transactionList('','','')
    .then(data => {
      console.log(data);
      this.transactions = data;
      this.loading = false;
    })
    .catch(error => { 
      console.log(error);
    });
  }
}

// Remote server
// const server_name = 'http://192.168.0.139:8000/';
// Localhost
const server_name = 'https://jovs5zmau3.execute-api.eu-west-1.amazonaws.com/prod/';

export const global = {
   // Service provider url
   API_TRANSACTIONS: server_name + 'transactions/'
}

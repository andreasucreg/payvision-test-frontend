import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import {AccordionModule} from "ngx-accordion";

import { AppComponent } from './app.component';
import { MasterPageComponent } from '../shared/master-page/master-page.component';
import { TransactionsComponent } from '../pages/transactions/transactions.component';
import { TransactionsService } from '../providers/transactions.service';
import { LoaderComponent } from '../shared/loader/loader.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "/assets/i18n/", ".json");
}


@NgModule({
  declarations: [
    AppComponent,
    MasterPageComponent,
    TransactionsComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AccordionModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    AppRoutingModule
  ], 
  providers: [
    TransactionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

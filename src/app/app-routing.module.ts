import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MasterPageComponent } from '../shared/master-page/master-page.component';
import { TransactionsComponent } from '../pages/transactions/transactions.component';


const appRoutes: Routes = [
    { path: '', component: MasterPageComponent, children: [
        { path: '', component: TransactionsComponent },
    ]}
]


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}


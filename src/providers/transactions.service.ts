import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { global } from '../app/global';

@Injectable()
export class TransactionsService {
    private url: string;
    private transactions_url = global.API_TRANSACTIONS

    constructor(
        private http: HttpClient,
    ) {}

    transactionList(action: any, currencyCode: string, orderBy: any) {
        this.url = this.transactions_url;
        const httpParams = new HttpParams();

        if(action != '' && action != '-1' && action != undefined) {
            this.url = this.url + '?action=' + action;
            if(currencyCode != '' && currencyCode != '-1' && currencyCode != undefined) {
                this.url = this.url + '&currencyCode=' + currencyCode;
                
                if(orderBy != '' && orderBy != '-1' && orderBy != undefined) {
                    this.url = this.url + '&orderBy=' + orderBy;
                }
            } else if(orderBy != '' && orderBy != '-1' && orderBy != undefined) {
                this.url = this.url + '&orderBy=' + orderBy;
            }
        }
        else if(currencyCode != '' && currencyCode != '-1' && currencyCode != undefined) {
            this.url = this.url + '?currencyCode=' + currencyCode;
            if(orderBy != '' && orderBy != '-1'  && orderBy != undefined) {
                this.url = this.url + '&orderBy=' + orderBy;
            }
        }
        else if(orderBy != '' &&  orderBy != '-1' && orderBy != undefined) {
            this.url = this.url + '?orderBy=' + orderBy;
        }

        

        const httpHeader = new HttpHeaders()
        .append('Authorization', 'Basic ' + btoa('code-challenge:payvisioner'));

        return this.http.get(this.url, { headers: httpHeader,
            params: httpParams
        }).toPromise();
    }

}

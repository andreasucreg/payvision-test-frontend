import { Component, Input, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-master-page',
  templateUrl: './master-page.component.html',
  styleUrls: ['./master-page.component.css']
})
export class MasterPageComponent implements OnInit {
  // Estos cambialos por la respuesta de un servicio que traiga los datos de usuario
  @Input() child_component: "";
  @Input() username: "";
  @Input() job_title: "";
  @Input() department: "";
  
  constructor(
    private translate: TranslateService,
  ) {
      translate.addLangs(["en", "es"]);
      //translate.setDefaultLang("en");

      let browserlang = translate.getBrowserLang();
      translate.use(browserlang.match(/en|es/) ? browserlang : "en");
  }

	ngOnInit() {
	}

	changeLanguage(lang) {
		this.translate.use(lang)
	}

}

# Payvision Test

This project contains the frontend developer test for Payvision and was developed in Angular.

## Installation

To run this application we first open our second terminal in the payvision-test-frontend folder. Then, check if react is installed in your computer. If not then use the following command:

<code>npm install -g @angular/cli</code>

Then, install dependencies: 
 
<code>npm install</code>

After this run the application with: 

<code>npm start</code>
